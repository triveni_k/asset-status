"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NodeCouchDb = require('node-couchdb');
// node-couchdb instance with default options
var connectionManager = /** @class */ (function () {
    function connectionManager() {
        this.couch = new NodeCouchDb();
        // node-couchdb instance with Memcached
        this.MemcacheNode = require('node-couchdb-plugin-memcached');
        this.couchWithMemcache = new NodeCouchDb({
            cache: new this.MemcacheNode
        });
        // node-couchdb instance talking to external service
        this.couchExternal = new NodeCouchDb({
            host: 'couchdb.external.service',
            protocol: 'https',
            port: 6984
        });
        // not admin party
        this.couchAuth = new NodeCouchDb({
            auth: {
                user: 'triveni.k@intimetec.com',
                pass: 'ITT@123456'
            }
        });
    }
    return connectionManager;
}());
exports.connectionManager = connectionManager;
//# sourceMappingURL=connectionManager.js.mapnpm
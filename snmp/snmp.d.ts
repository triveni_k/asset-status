import { ipAndOids } from "../db/models/ipsAndOids";
export declare class Snmp {
    getSnmpConnection(ip: string): any;
    getAssetProperties(session: any, ipAndOid: ipAndOids): Promise<{}>;
    closeSnmpConnection(): void;
}

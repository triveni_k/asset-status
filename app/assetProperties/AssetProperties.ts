import {Snmp} from "../../snmp/snmp";
import {ipAndOids} from "../../db/models/ipsAndOids";
import {AssetProperty} from "../../db/models/session";
let AssetPropertiesHelper = new (require("./AssetPropertiesHelper").AssetPropertiesHelper);

let async = require("async");
let snmp = new Snmp();

export class AssetProperties {
  static ipsAndOids: Array<ipAndOids>;
  static assetProperty: Array<AssetProperty>;
  constructor(){
    AssetProperties.ipsAndOids = AssetProperties.getIpsAndOids();
    AssetProperties.assetProperty = AssetPropertiesHelper.createSession([AssetProperties.ipsAndOids]);
  }

  async getAssetProperties() {
     AssetProperties.assetProperty = AssetPropertiesHelper.getAssetProperties(AssetProperties.assetProperty);
    }

  static getIpsAndOids(): Array<ipAndOids>{
    return
  }
}
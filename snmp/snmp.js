"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var promisify = require('util').promisify;
var snmp = require("net-snmp");
var session;
var Snmp = /** @class */ (function () {
    function Snmp() {
    }
    Snmp.prototype.getSnmpConnection = function (ip) {
        session = snmp.createSession(ip, "public");
        return session;
    };
    Snmp.prototype.getAssetProperties = function (session, ipAndOid) {
        return new Promise(function (resolve, reject) {
            session.get(ipAndOid, function (error, varbinds) {
                if (error) {
                    console.log("11111111", error);
                }
                else {
                    for (var i = 0; i < varbinds.length; i++)
                        if (snmp.isVarbindError(varbinds[i])) {
                            console.log("22222222");
                        }
                        else {
                            console.log("3333333", varbinds);
                            resolve(varbinds);
                        }
                }
            });
        });
    };
    Snmp.prototype.closeSnmpConnection = function () {
        snmp.closeConnection();
    };
    return Snmp;
}());
exports.Snmp = Snmp;
//# sourceMappingURL=snmp.js.map
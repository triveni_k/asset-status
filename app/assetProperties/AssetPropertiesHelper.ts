import {ipAndOids} from "../../db/models/ipsAndOids";
import {AssetProperty} from "../../db/models/session";
import {OidAndValue} from "../../db/models/oidAndValue";

let snmp = require("../../snmp/snmp").Snmp;
let snmpObj = new snmp();

export class AssetPropertiesHelper {
  createSession(ipsAndOids: Array<ipAndOids>) {
    let assetProperties: Array<AssetProperty> = [];
    for (let index = 0; index < ipsAndOids.length; index++) {
      let assetProperty: AssetProperty = {};
      assetProperty.ip = ipsAndOids[index].ip;
      assetProperty.oids = ipsAndOids[index].oids;
      assetProperty.session = snmpObj.getSnmpConnection(ipsAndOids[index].ip);
      assetProperties.push(assetProperty);
    }
    return assetProperties;
  }


  async getAssetProperties(assetProperties: Array<AssetProperty>) {
    for (let index = 0; index < assetProperties.length; index++) {
      let oidsAndValue: OidAndValue;
      oidsAndValue = await snmpObj.getAssetProperties(assetProperties[index].session, assetProperties[index].oids);
      assetProperties[index].oidsAndValue.oid = oidsAndValue.oid;
      assetProperties[index].oidsAndValue.value = oidsAndValue.value;
    }
    return assetProperties;
  }
}
export declare class Asset {
    insertData(couch: any, options: any, dbName: any): void;
    getData(): void;
    updateOids(): void;
    createDb(couch: any): void;
}

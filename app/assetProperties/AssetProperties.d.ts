import { ipAndOids } from "../../db/models/ipsAndOids";
import { AssetProperty } from "../../db/models/session";
export declare class AssetProperties {
    static ipsAndOids: Array<ipAndOids>;
    static assetProperty: Array<AssetProperty>;
    constructor();
    getAssetProperties(): Promise<void>;
    static getIpsAndOids(): Array<ipAndOids>;
}

export declare class Asset {
    assetProperties: any;
    ip: string;
    macAddress: string;
    sku: string;
    oids: object;
    constructor(ip: string);
}

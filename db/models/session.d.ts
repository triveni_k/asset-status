import { OidAndValue } from "./oidAndValue";
export declare class AssetProperty {
    session: any;
    ip: string;
    oidsAndValue: OidAndValue;
    oids: Array<string>;
}

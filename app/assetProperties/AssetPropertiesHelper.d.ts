import { ipAndOids } from "../../db/models/ipsAndOids";
import { AssetProperty } from "../../db/models/session";
export declare class AssetPropertiesHelper {
    createSession(ipsAndOids: Array<ipAndOids>): AssetProperty[];
    getAssetProperties(assetProperties: Array<AssetProperty>): Promise<AssetProperty[]>;
}

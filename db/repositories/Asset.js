"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var connectionManager_1 = require("../connectionManager");
var Asset = /** @class */ (function () {
    function Asset() {
    }
    Asset.prototype.insertData = function (couch, options, dbName) {
        couch.insert(dbName, options).then(function (_a) {
            var data = _a.data, headers = _a.headers, status = _a.status;
        }, function (err) {
        });
    };
    Asset.prototype.getData = function () {
    };
    Asset.prototype.updateOids = function () {
    };
    Asset.prototype.createDb = function (couch) {
        couch.createDatabase("Asset").then(function () {
            console.log("db created");
        }, function (err) {
            console.log("error", err);
        });
    };
    return Asset;
}());
exports.Asset = Asset;
var s = new connectionManager_1.connectionManager(), m = new Asset();
m.createDb(s.couchAuth);
//# sourceMappingURL=Asset.js.map